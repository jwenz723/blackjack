using blackJack.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace blackJack
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        

        #region Properties
        /// <summary>
        /// This is the hand that the player plays with
        /// </summary>
        public UserHand playerHand;

        /// <summary>
        ///  This is the hand that the dealer plays with
        /// </summary>
        public UserHand dealerHand;

        /// <summary>
        /// This is the deck that is being used for play
        /// </summary>
        public Deck deckinuse;

		/// <summary>
        /// Public bool to track whether the bid window was closed by the user. Useful to avoid diplicate closing questions
        /// for user
        /// </summary>
        public bool bidWindowClosedByUser = false;
    

        #endregion

        #region Constructor

        public MainWindow()
        {
            InitializeComponent();
            //Centers the window to the middle of the screen
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            //Calls the start bid window method from the MainWindowViewModel
            getViewModel().StartBidWindow();  
        
        }

        #endregion

        #region Button Click Functions
        /// <summary>
        /// If the user is not happy with their bets, they can reset the bets back to 0, and the current balance of the game back to
        /// their initial gambling balance
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnResetBet_Click(object sender, RoutedEventArgs e)
        {
            getViewModel().resetBid();
        }
        /// <summary>
        /// called when clicking the deal button. creates the deck and passes it to user. checks for blackjack
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_deal_Click(object sender, RoutedEventArgs e)
        {
            //Disable controls the user should not have access to after they click on Deal
            sp_chips.IsEnabled = false;
            btnResetBet.IsEnabled = false;

            // Enable player controls, and disable deal.
            button_hit.IsEnabled = true;
            button_stand.IsEnabled = true;
            button_ddown.IsEnabled = true;
            button_deal.IsEnabled = false;

            // create the deck and player hands. 
            deckinuse = new Deck();
            playerHand = new UserHand(deckinuse.dealCard(), deckinuse.dealCard());
            dealerHand = new UserHand(deckinuse.dealCard(), deckinuse.dealCard());

            //if the face value of both cards are the same, allow spliting. 
            if (playerHand.cards[0].faceValue == playerHand.cards[1].faceValue)
            {
                button_split.IsEnabled = true;
            }

            //if the dealer is showing and ace, allow surrender
            if (dealerHand.cards[1].faceValue == 1)
            {
                btn_surrender.IsEnabled = true;
            }

            //set player hand worth and display to text box
            playerHand.handWorth = playerHand.cards[0].cardWorth + playerHand.cards[1].cardWorth;
            // txt_playerHandWorth.Text = playerHand.handWorth.ToString();
            lblPlayerHandWorth.Content = playerHand.handWorth.ToString();
            

            //display hand worth for the 1 showing card. 
            lblDealerHandWorthValue.Content = dealerHand.cards[1].cardWorth.ToString();

            // Set the content to display in the visual cards on the GUI
            playerhandCard1.DataContext = playerHand.cards[0];
            playerhandCard2.DataContext = playerHand.cards[1];
            dealerhandCard2.DataContext = dealerHand.cards[1];

            //if the opening hand for the player is 21, then stand. 

            //this creates a bug that makes the card animations occur after the winning message has been displayed
            if (playerHand.handWorth == 21)
            {
                DealerTurn();
            }
            
        }
        /// <summary>
        /// calls the hit method when clicked. checks for bust.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_hit_Click(object sender, RoutedEventArgs e)
        {
            PlayerHit();
            if (IsBust(playerHand))
            {
                getViewModel().PlayerLose();  
            }
        }
        /// <summary>
        /// calls the stand method when clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_stand_Click(object sender, RoutedEventArgs e)
        {
            PlayerStand();
        }
        /// <summary>
        /// calls the double down method when clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_ddown_Click(object sender, RoutedEventArgs e)
        {
            PlayerDoubleDown();
        }
        /// <summary>
        /// called when user clicks surrender button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_surrender_Click(object sender, RoutedEventArgs e)
        {
            PlayerSurrender();
            startTurn();
        }
 

        /// <summary>
        /// Prompts the user with a messagebox asking them if they really do want to restart the application.
        /// if yes, Shuts down the current application (including shutting off debugging) and restarts the application using the exe. sets
        /// bidWindowClosedByUser to true to trick application so user does not get additional message box.
        /// If user clicks no, cancels the close event and continues running of the application
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_restart_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Restart game?", "Question", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.No)
            {
                return;
            }
            else
            {
                //tricks the close event to think bid window was closed by user. this is to bypass an additional messagebox when
                //the close event is hit by the application
                this.bidWindowClosedByUser = true;
                System.Diagnostics.Process.Start(Application.ResourceAssembly.Location);
                Application.Current.Shutdown();
            }
        }
        /// <summary>
        /// called when the quit button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_quit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        #endregion

        #region Function Definitions

        /// <summary>
        /// this function animates the card that was just dealt to the player onto the screen
        /// </summary>
        public void animatePlayerCard()
        {
            // get the index value of the card that was just dealt to the player
            int currentCardNumber = playerHand.cards.Count;

            // update the card on the screen
            switch (currentCardNumber)
            {
                case 3:
                    playerhandCard3.DataContext = playerHand.cards[2];
                    break;
                case 4:
                    playerhandCard4.DataContext = playerHand.cards[3];
                    break;
                case 5:
                    playerhandCard5.DataContext = playerHand.cards[4];
                    break;
                case 6:
                    playerhandCard6.DataContext = playerHand.cards[5];
                    break;
                case 7:
                    playerhandCard7.DataContext = playerHand.cards[6];
                    break;
                case 8:
                    playerhandCard8.DataContext = playerHand.cards[7];
                    break;
                case 9:
                    playerhandCard9.DataContext = playerHand.cards[8];
                    break;
            }

            // animate a card onto the screen
            Storyboard sb = this.FindResource("hitCard" + currentCardNumber) as Storyboard;
            sb.Begin();
        }

        /// <summary>
        /// this function animates the card that was just dealt to the dealer onto the screen
        /// </summary>
        public void animateDealerCard()
        {
            // get the index value of the card that was just dealt to the player
            int currentCardNumber = dealerHand.cards.Count;

            // update the card on the screen
            switch (currentCardNumber)
            {
                case 3:
                    dealerhandCard3.DataContext = dealerHand.cards[2];
                    break;
                case 4:
                    dealerhandCard4.DataContext = dealerHand.cards[3];
                    break;
                case 5:
                    dealerhandCard5.DataContext = dealerHand.cards[4];
                    break;
                case 6:
                    dealerhandCard6.DataContext = dealerHand.cards[5];
                    break;
                case 7:
                    dealerhandCard7.DataContext = dealerHand.cards[6];
                    break;
                case 8:
                    dealerhandCard8.DataContext = dealerHand.cards[7];
                    break;
                case 9:
                    dealerhandCard9.DataContext = dealerHand.cards[8];
                    break;
            }

            // only animate if not the initial 2 cards
            if (currentCardNumber > 2)
            {
                // animate a card onto the screen
                Storyboard sb = this.FindResource("dealerHit" + currentCardNumber) as Storyboard;
                sb.Begin();
            }
            
        }

        /// <summary>
        /// This function does the necessary work to initialize the table objects and clear all 
        /// previous bets and hands.  It will disable all buttons except for the chips so that
        /// the user is required to enter a bet.
        /// </summary>
        public void startTurn()
        {
            // make the dealer's handworth box hidden
            lblDealerHandWorthValue.Visibility = Visibility.Hidden;
            lblDealerHandWorth.Visibility = Visibility.Hidden;

            //disabling all controls except for the chips, restart, and exit.
            button_deal.IsEnabled = false;
            button_hit.IsEnabled = false;
            button_split.IsEnabled = false;
            button_stand.IsEnabled = false;
            button_ddown.IsEnabled = false;
            btn_surrender.IsEnabled = false;
            sp_controls.IsEnabled = false;
            sp_chips.IsEnabled = true;
            btnResetBet.IsEnabled = true;


            // clear the current bet
            getViewModel().CurrentUserBet = 0;

            // clear out the past hands
            playerHand = null;
            dealerHand = null;

            // move the cards off of the screen
            Storyboard sb = this.FindResource("moveCardsToStartingPosition") as Storyboard;
            sb.Begin();
        }

        /// <summary>
        /// This function executes the hit option and adds one card to the specified deck.
        /// </summary>
        public void PlayerHit()
        {
            // Draw card from pile
            Card temp = deckinuse.dealCard();

            // Add to player's hand
            playerHand.addCardToHand(temp);

            // Update the total hand value
            //int tempworth = Convert.ToInt16(txt_playerHandWorth.Text);
            //tempworth += temp.cardWorth;
            //txt_playerHandWorth.Text = tempworth.ToString();
            int tempworth = Convert.ToInt16(lblPlayerHandWorth.Content);
            tempworth += temp.cardWorth;
            lblPlayerHandWorth.Content = tempworth.ToString();

            // animate a card onto the screen
            animatePlayerCard();
        }

        /// <summary>
        /// This function executes the surrender option
        /// </summary>
        public void PlayerSurrender()
        {
            // divide bet by two
            // add the result to the balance
            double temp = getViewModel().CurrentUserBet / 2;
            getViewModel().CurrentUserBalance += temp;
        }

        /// <summary>
        /// This function ends the player's turn and starts the dealer's turn
        /// </summary>
        public void PlayerStand()
        {
            DealerTurn();
        }

        /// <summary>
        /// This function executes the double down option
        /// </summary>
        public void PlayerDoubleDown()
        {
            if ((getViewModel().CurrentUserBalance - getViewModel().CurrentUserBet) < 0)
            {
                //not able to bet that much
                MessageBox.Show("You can't bet that much! You only have $" + getViewModel().CurrentUserBalance + " left.");
            }
            else
            {
                //subtract from user balance the total amount bet, double the bet amount
                getViewModel().CurrentUserBalance -= getViewModel().CurrentUserBet;
                getViewModel().CurrentUserBet *= 2;
                playerHand.addCardToHand(deckinuse.dealCard());

                // animate the card onto the screen
                animatePlayerCard();

                // update player total hand worth to text box
                //txt_playerHandWorth.Text = playerHand.handWorth.ToString();
                lblPlayerHandWorth.Content = playerHand.handWorth.ToString();

                //if hand is a bust, player loses, else dealer turn
                if (IsBust(playerHand))
                {
                    getViewModel().PlayerLose();
                }
                else
                {
                    DealerTurn();

                }
            }       
        }

        /*---------- IMPORTANT NOTE: DealerTurn is when the player clicks on stand or double down----------*/
        /// <summary>
        /// This function executes the dealer's turn
        /// </summary>
        /// <returns>A code message that indicates the results of the process</returns>
        public void DealerTurn()
        {
            // flip over the dealer's first card
            dealerHandCard1.DataContext = dealerHand.cards[0];
            Storyboard sb = this.FindResource("flipDealerCard1") as Storyboard;
            sb.Begin();

            // Change the text of the stand button
            button_stand.Content = "Dealer's Turn...";

            // disable some buttons
            button_hit.IsEnabled = false;
            button_ddown.IsEnabled = false;

            // show the dealer hand worth labels
            lblDealerHandWorthValue.Visibility = Visibility.Visible;
            lblDealerHandWorth.Visibility = Visibility.Visible;

            //if dealer's hand is already stronger, stay
            if (dealerHand.handWorth < playerHand.handWorth)
            {
                //loop until hand worth is 17 or more
                while (dealerHand.handWorth < 17)
                {
                    // add a card to the dealers hand
                    dealerHand.addCardToHand(deckinuse.dealCard());

                    // animate a card onto the screen
                    animateDealerCard();
                }
            }

            // show the dealer's hand worth
            lblDealerHandWorthValue.Content = dealerHand.handWorth.ToString();

            // Use a background worker to update the total cost of the invoice in the database
            BackgroundWorker checkForWin = new BackgroundWorker();
            checkForWin.DoWork += new DoWorkEventHandler(getViewModel().checkForWin_DoWork);
            checkForWin.RunWorkerCompleted += new RunWorkerCompletedEventHandler(getViewModel().checkForWin_RunWorkerCompleted);
            checkForWin.RunWorkerAsync();


        }
                
        /// <summary>
        /// This function checks the player's hands and
        /// determines if the hand is a bust or not.
        /// </summary>
        /// <returns>A bool; true for bust and false for no bust</returns>
        /// <param name="hand">The Deck to test bust against</param>
        public bool IsBust(UserHand hand)
        {
            // Check to see if Player's handworth is greater than 21
            // If it is greater than 21, then it's a bust
            // else it is not a bust
            if (hand.handWorth > 21)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// This function compares the dealer's hand and the player's hand
        /// and does the correct calculation based on the results of the comparison
        /// </summary>
        public void Showdown()
        {
            if (dealerHand.handWorth > playerHand.handWorth)
            {
                getViewModel().PlayerLose();
            }
            else if (dealerHand.handWorth == playerHand.handWorth)
            {
                PlayerPush();
            }
            else
            {
                getViewModel().PlayerWin();
            }
        }

        /// <summary>
        /// This function will return the players bet due to a draw condition. 
        /// </summary>
        public void PlayerPush()
        {
            MessageBox.Show("You pushed the dealer. How's that for a waste of time?", "Push", MessageBoxButton.OK, MessageBoxImage.Warning);

            //return the users bet with no winnings. 
            getViewModel().CurrentUserBalance += getViewModel().CurrentUserBet;
            getViewModel().CurrentUserBet = 0;
            lblPlayerHandWorth.Content = 0;
            lblDealerHandWorthValue.Content = 0;

            startTurn();

        }

        #endregion

        #region Poker Chip Click Events
        /// <summary>
        /// click event for poker chip. updates user balance and bet balance and displays error if not enough money to bet.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_chip_1_Click(object sender, RoutedEventArgs e)
        {
            getViewModel().pokerChipClick(1);
        }
        /// <summary>
        /// click event for poker chip. updates user balance and bet balance and displays error if not enough money to bet.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_chip_5_Click(object sender, RoutedEventArgs e)
        {
            getViewModel().pokerChipClick(5);
        }
        /// <summary>
        /// click event for poker chip. updates user balance and bet balance and displays error if not enough money to bet.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_chip_10_Click(object sender, RoutedEventArgs e)
        {
            getViewModel().pokerChipClick(10);
        }
        /// <summary>
        /// click event for poker chip. updates user balance and bet balance and displays error if not enough money to bet.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_chip_25_Click(object sender, RoutedEventArgs e)
        {
            getViewModel().pokerChipClick(25);
        }
        /// <summary>
        /// click event for poker chip. updates user balance and bet balance and displays error if not enough money to bet.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_chip_50_Click(object sender, RoutedEventArgs e)
        {
            getViewModel().pokerChipClick(50);
        }
        #endregion

        /// <summary>
        /// Asks the user if they want to quit when trying to close the main window. if yes, program ends. If no, cancels the closing event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void window_table_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //Stops duplication of asking the user if they want to quit. Since they already quit the bid window, they want to quit the application
            if (bidWindowClosedByUser == true)
            {
                e.Cancel = false;
            }
            else
            {
                if (MessageBox.Show("Quit Playing?", "Question", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.No)
                {
                    e.Cancel = true;
                }
                else
                {
                    e.Cancel = false;
                }
            }
        }
        /// <summary>
        /// method created to reduce confusion of view model casting by returning an already casted method to access variables in the
        /// main window view model
        /// </summary>
        /// <returns></returns>
        private MainWindowViewModel getViewModel()
        {
            return (MainWindowViewModel)this.DataContext;

        }

    }
}
