﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace blackJack
{
    /// <summary>
    /// Interaction logic for BidWindow.xaml
    /// </summary>
    public partial class BidWindow : Window
    {
        /// <summary>
        /// Public bool to determine whether the user is allowed to close the window or not.
        /// </summary>
        public bool allowedToClose = false;
        public BidWindow()
        {
            InitializeComponent();
            //Brings window to center of screen
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            //Sets focus to the textbox so the user doesn't have to click inside of it to get started.
            txtStartingBalance.Focus();
        }
        /// <summary>
        /// Checks to see if user entered a non int, or a value less than or equal to 0. If so, message box an error.
        /// If input is valid, allow window to close and pass information to main window.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            int temp = 0;
            if(!Int32.TryParse(this.txtStartingBalance.Text, out temp) || Convert.ToInt32(txtStartingBalance.Text) <= 0)
            {
                MessageBox.Show("Please enter a starting balance greater than 0.");
                return;
            }
            else
            {
                this.allowedToClose = true;
                this.Close();
            }
           
        }
        /// <summary>
        /// Checks for key presses. If it is not Enter, return. If it is Enter, treat the same as if you clicked on the button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtStartingBalance_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Return)
            {
                int temp = 0;
                if (!Int32.TryParse(this.txtStartingBalance.Text, out temp) || Convert.ToInt32(txtStartingBalance.Text) <= 0)
                {
                    MessageBox.Show("Please enter a starting balance greater than 0.");
                    return;
                }
                else
                {
                    this.allowedToClose = true;
                    this.Close();
                }
            }
            else
            {
                return;
            }
        }
        /// <summary>
        /// Used to force the user to either quit the program, or enter something into the bid box. If they choose to quit,
        /// shuts down the application, and visits main window to finish out the method. Main window has a return statement to halt finishing
        /// of the method, and the program ends successfully, otherwise the closing event is cancelled.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (this.allowedToClose == true)
            {
                e.Cancel = false;
            }

            else
            {
                if(MessageBox.Show("Quit Playing?", "Question", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.No)
                {
                    e.Cancel = true;
                }
                else
                {
                    Application.Current.Shutdown();
                }
            }
                
        }
    }
}
