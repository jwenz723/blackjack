﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Animation;

namespace blackJack.ViewModels
{
    public class MainWindowViewModel : ViewModelBase
    {
        /// <summary>
        /// private reference to the bid window view model
        /// </summary>
        private BidWindowViewModel bidWindowVM = new BidWindowViewModel();
        /// <summary>
        ///public reference to the bid window view model
        /// </summary>
        public BidWindowViewModel BidWindowVM
        {
            get
            {
                return bidWindowVM;
            }
            set
            {
                bidWindowVM = value;
                OnPropertyChanged("BidWindowVM");
            }
        }
        /// <summary>
        /// private reference to the current user balance
        /// </summary>
        private double currentUserBalance;
        /// <summary>
        /// public reference to the current user balance
        /// </summary>
        public double CurrentUserBalance
        {
            get
            {
                return currentUserBalance;
            }
            set
            {
                currentUserBalance = value;
                OnPropertyChanged("CurrentUserBalance");
            }
        }


        /// <summary>
        /// creates private int variable to keep track of the starting user balance
        /// </summary>
        private int startingUserBalance = -1;
        /// <summary>
        /// function that will be called to set the current balance to the balance pulled from the bid window, and store
        /// the same balance in a starting user balance that we can later use to clear the bet and re-set our balance
        /// </summary>
        /// <param name="startingBalance"></param>
        public void setStartingBalance(int startingBalance)
        {
            if (startingUserBalance < 0 && startingBalance > 0)
            {
                this.CurrentUserBalance = startingBalance;
                this.startingUserBalance = startingBalance;
                //Sets the initial bet to 0 since the game is just starting.
                this.CurrentUserBet = 0;
            }
        }
        /// <summary>
        /// method that resets the current users balance to the starting balance if clicking reset button
        /// </summary>
        public void resetBid()
        {
            this.CurrentUserBalance = this.startingUserBalance;
            this.CurrentUserBet = 0;
        }
        /// <summary>
        /// private reference to the current user bet
        /// </summary>
        private double currentUserBet;
        /// <summary>
        /// public reference to the current user bet
        /// </summary>
        public double CurrentUserBet
        {
            get
            {
                return currentUserBet;
            }
            set
            {
                currentUserBet = value;
                OnPropertyChanged("CurrentUserBet");
            }
        }
        /// <summary>
        /// Method to abstract business logic from main window. receives the input from the main window and passes it into the method.
        /// casts the current window to a main window to access controls so they can be disabled. uses error handling to check
        /// if the user has a sufficient balance to continue playing. updates the balance for bet and bid.
        /// </summary>
        /// <param name="chipAmount"></param>
        public void pokerChipClick(int chipAmount)
        {
            Func<MainWindow> fnc = delegate() { return Application.Current.Windows.Cast<Window>().FirstOrDefault(wnd => wnd is MainWindow) as MainWindow; };
            MainWindow mw = Application.Current.Dispatcher.Invoke(fnc) as MainWindow;
            //re-enables the controls for the deal button and the panel the other buttons are in i.e. hit, stay, etc. Does NOT re-enable the
            //actual hit, bid etc. buttons. just the panel.
            mw.sp_controls.IsEnabled = true;
            mw.button_deal.IsEnabled = true;   
            //Error handing to make sure that the current balance minus the requested bet does not go below 0
            if (this.CurrentUserBalance - chipAmount < 0)
            {
                MessageBox.Show("Cannot bet $"+ chipAmount + ". You have $" + this.CurrentUserBalance + " left.");
                return;
            }
            //Updates the current user balance and bet with databinding
            this.CurrentUserBalance = this.CurrentUserBalance - chipAmount;
            this.CurrentUserBet = this.CurrentUserBet + chipAmount;
        }
        /// <summary>
        /// Method that is called in the constructor to start the bid window. disables all relevant controls, initializes a balance, enables
        /// the chip controls, and tells the user to place a bet
        /// </summary>
        public void StartBidWindow()
        {
            Func<MainWindow> fnc = delegate() { return Application.Current.Windows.Cast<Window>().FirstOrDefault(wnd => wnd is MainWindow) as MainWindow; };
            MainWindow mw = Application.Current.Dispatcher.Invoke(fnc) as MainWindow;
            //disabling all controls except restart and exit.
            mw.button_deal.IsEnabled = false;
            mw.button_hit.IsEnabled = false;
            mw.button_split.IsEnabled = false;
            mw.button_stand.IsEnabled = false;
            mw.button_ddown.IsEnabled = false;
            mw.btn_surrender.IsEnabled = false;
            mw.sp_controls.IsEnabled = false;
            mw.sp_chips.IsEnabled = false;
            //new instance of the bid window  
            BidWindow bw = new BidWindow();
            bw.DataContext = this.BidWindowVM;
            //opens the bid window
            bw.ShowDialog();
            //The user chose to quit the program. return so we do not enable controls or display message box.
            if (bw.allowedToClose == false)
            {
                mw.bidWindowClosedByUser = true;
                return;
            }
            //initializes the user balance to 0
            this.CurrentUserBalance = 0;
            //sets the current balance to the input enter into the textbox through the bid window
            this.setStartingBalance(this.BidWindowVM.UserBuyin);
            //after bid window is closed, enables the chips control
            mw.sp_chips.IsEnabled = true;
            //tells the user to click on a poker chip to initialize a bet. Tells the user their buy in balance.
            MessageBox.Show("Your buy in was: $" + BidWindowVM.UserBuyin + ". Please click on a poker chip to place your bet.");
        }
        /// <summary>
        /// Method that is called if the user wins the game. Does appropriate math depending on if user gets blackjack or wins regularly
        /// </summary>
        public void PlayerWin()
        {
            Func<MainWindow> fnc = delegate() { return Application.Current.Windows.Cast<Window>().FirstOrDefault(wnd => wnd is MainWindow) as MainWindow; };
            MainWindow mw = Application.Current.Dispatcher.Invoke(fnc) as MainWindow;
            //check to see if user had blackjack
            if (mw.playerHand.cards.Count == 2 && mw.playerHand.handWorth == 21)
            {
                if (mw.playerHand.cards[0].cardWorth == 11 || mw.playerHand.cards[1].cardWorth == 11)
                {
                    this.CurrentUserBalance += this.CurrentUserBet * 2.5;
                    this.CurrentUserBet = this.CurrentUserBet * 2.5;
                    MessageBox.Show("BLACK JACK! You gained $" + this.CurrentUserBet + ". Your new total balance is $" + this.CurrentUserBalance + ".", "Question", MessageBoxButton.OK, MessageBoxImage.Warning);

                }
            }
            else
            {
                this.CurrentUserBalance += this.CurrentUserBet * 2;
                this.CurrentUserBet = this.CurrentUserBet * 2;
                MessageBox.Show("You win! You gained $" + this.CurrentUserBet + ". Your new total balance is $" + this.CurrentUserBalance + ".", "Question", MessageBoxButton.OK, MessageBoxImage.Warning);

            }
            this.CurrentUserBet = 0;
            mw.lblPlayerHandWorth.Content = "0";
            mw.startTurn();
        }
        /// <summary>
        /// Method that the main window calls if the user loses. Displays loss depending on the type of loss it was
        /// </summary>
        public void PlayerLose()
        {
            Func<MainWindow> fnc = delegate() { return Application.Current.Windows.Cast<Window>().FirstOrDefault(wnd => wnd is MainWindow) as MainWindow; };
            MainWindow mw = Application.Current.Dispatcher.Invoke(fnc) as MainWindow;
            if (mw.IsBust(mw.playerHand))
            {
                if(this.CurrentUserBalance == 0)
                {
                    MessageBox.Show("BUST! You went over 21. Your total amount lost was $" + this.CurrentUserBet + ". You have $" + this.CurrentUserBalance + " left. You are now out of money. Thanks for playing!", "Question", MessageBoxButton.OK, MessageBoxImage.Warning);
                    mw.bidWindowClosedByUser = true;
                    Application.Current.Shutdown();
                }
                MessageBox.Show("BUST! You went over 21. Your total amount lost was $" + this.CurrentUserBet + ". You have $" + this.CurrentUserBalance + " left.", "Question", MessageBoxButton.OK, MessageBoxImage.Warning);
                mw.lblPlayerHandWorth.Content = "0";
            }
            else
            {
                if (this.CurrentUserBalance == 0)
                {
                    MessageBox.Show("Your total amount lost was $" + this.CurrentUserBet + ". You have $" + this.CurrentUserBalance + " left. You are now out of money. Thanks for playing!", "Question", MessageBoxButton.OK, MessageBoxImage.Warning);
                    mw.bidWindowClosedByUser = true;
                    Application.Current.Shutdown();
                }
                MessageBox.Show("Sorry, but you lose. Your total amount lost was $" + this.CurrentUserBet + ". You have $" + this.CurrentUserBalance + " left.", "Question", MessageBoxButton.OK, MessageBoxImage.Warning);
                this.CurrentUserBet = 0;
                mw.lblPlayerHandWorth.Content = "0";
            }
            mw.startTurn();
        }

      
        /// <summary>
        /// puts the thread to sleep to check for a win
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void checkForWin_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                System.Threading.Thread.Sleep(2000);
            }
            catch (Exception ex)
            {
                //Just throw the exception
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }
        /// <summary>
        /// after thread finished, this function is called to enable the stand button content, and check if the player wins, or moves to showdown
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void checkForWin_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Func<MainWindow> fnc = delegate() { return Application.Current.Windows.Cast<Window>().FirstOrDefault(wnd => wnd is MainWindow) as MainWindow; };
            MainWindow mw = Application.Current.Dispatcher.Invoke(fnc) as MainWindow;
            try
            {
                // return the text back to default
                mw.button_stand.Content = "Stand";

                //if dealer hand worth is bust, player wins; else showdown time!
                if (mw.IsBust(mw.dealerHand))
                {
                    PlayerWin();
                }
                else
                {
                    mw.Showdown();
                }
            }
            catch (Exception ex)
            {
                //Just throw the exception
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

    }
}
