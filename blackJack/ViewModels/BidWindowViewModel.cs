﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace blackJack.ViewModels
{
    public class BidWindowViewModel : ViewModelBase
    {
        public void OnWindowClosing(object sender, ConsoleCancelEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private int userBuyin;
        public int UserBuyin
        {
            get
            {
                return userBuyin;
            }
            set
            {
                userBuyin = value;
                OnPropertyChanged("UserBuyin");
            }
        }
    }
}
