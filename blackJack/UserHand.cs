﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace blackJack
{
    public class UserHand
    {
        public List<Card> cards { get; private set; }

        public int handWorth { get; set; }

        /// <summary>
        /// Default Constructor
        /// </summary>
        public UserHand(Card firstcard, Card secondCard)
        {
            // instantiate the cards
            cards = new List<Card>();

            // set the starting hand worth
            handWorth = 0;

            // Add the first 2 cards into the hand
            addCardToHand(firstcard);
            addCardToHand(secondCard);
        }

        /// <summary>
        /// This function will add a card into the player hand
        /// </summary>
        /// <param name="cardToAdd">The card to add</param>
        public void addCardToHand(Card cardToAdd)
        {
            cards.Add(cardToAdd);

            handWorth += cardToAdd.cardWorth;
        }

        public void clearHand()
        {
            cards.Clear();
            handWorth = 0;
        }


    }
}
